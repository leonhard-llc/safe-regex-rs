// % sysctl -n machdep.cpu.brand_string
// Intel(R) Core(TM) i5-8259U CPU @ 2.30GHz (4 cores)
// % cargo +nightly bench --package bench
//    Compiling bench v0.0.0 (/Users/user/safe-regex-rs/bench)
//     Finished bench [optimized] target(s) in 1m 39s
//      Running benches/lib.rs (target/release/deps/lib-3b3a3d8523157e79)
//
// running 44 tests
// test capture10_regex                  ... bench:       2,606 ns/iter (+/- 176)
// test capture10_safe_regex             ... bench:       1,814 ns/iter (+/- 607)
// test datetime_capture_100_regex       ... bench:         211 ns/iter (+/- 11)
// test datetime_capture_100_safe_regex  ... bench:       1,874 ns/iter (+/- 125)
// test datetime_capture_10kb_regex      ... bench:      12,267 ns/iter (+/- 666)
// test datetime_capture_10kb_safe_regex ... bench:     187,599 ns/iter (+/- 8,686)
// test datetime_capture_1kb_regex       ... bench:       1,172 ns/iter (+/- 58)
// test datetime_capture_1kb_safe_regex  ... bench:      18,328 ns/iter (+/- 821)
// test datetime_parse_regex             ... bench:         387 ns/iter (+/- 100)
// test datetime_parse_safe_regex        ... bench:         276 ns/iter (+/- 10)
// test pem_base64_regex                 ... bench:         122 ns/iter (+/- 32)
// test pem_base64_safe_regex            ... bench:       4,972 ns/iter (+/- 202)
// test phone_capture_100kb_regex        ... bench:     183,993 ns/iter (+/- 25,498)
// test phone_capture_100kb_safe_regex   ... bench:   1,190,252 ns/iter (+/- 52,368)
// test phone_capture_10kb_regex         ... bench:      19,172 ns/iter (+/- 872)
// test phone_capture_10kb_safe_regex    ... bench:     120,779 ns/iter (+/- 23,936)
// test phone_capture_1kb_regex          ... bench:       2,043 ns/iter (+/- 119)
// test phone_capture_1kb_safe_regex     ... bench:      12,076 ns/iter (+/- 2,059)
// test phone_capture_1mb_regex          ... bench:   1,919,659 ns/iter (+/- 96,226)
// test phone_capture_1mb_safe_regex     ... bench:  12,209,546 ns/iter (+/- 489,670)
// test repeat10_regex                   ... bench:          34 ns/iter (+/- 1)
// test repeat10_safe_regex              ... bench:          89 ns/iter (+/- 21)
// test repeat20_regex                   ... bench:         187 ns/iter (+/- 61)
// test repeat20_safe_regex              ... bench:         378 ns/iter (+/- 89)
// test repeat30_regex                   ... bench:          66 ns/iter (+/- 3)
// test repeat30_safe_regex              ... bench:         951 ns/iter (+/- 41)
// test repeat_capture10_regex           ... bench:         230 ns/iter (+/- 73)
// test repeat_capture10_safe_regex      ... bench:         136 ns/iter (+/- 34)
// test repeat_capture20_regex           ... bench:         301 ns/iter (+/- 83)
// test repeat_capture20_safe_regex      ... bench:         533 ns/iter (+/- 174)
// test repeat_capture30_regex           ... bench:         371 ns/iter (+/- 96)
// test repeat_capture30_safe_regex      ... bench:       1,403 ns/iter (+/- 417)
// test string_search_100_regex          ... bench:          50 ns/iter (+/- 16)
// test string_search_100_safe_regex     ... bench:         440 ns/iter (+/- 15)
// test string_search_100kb_regex        ... bench:       3,185 ns/iter (+/- 834)
// test string_search_100kb_safe_regex   ... bench:     434,927 ns/iter (+/- 80,100)
// test string_search_10kb_regex         ... bench:         335 ns/iter (+/- 96)
// test string_search_10kb_safe_regex    ... bench:      44,353 ns/iter (+/- 2,167)
// test string_search_1kb_regex          ... bench:          53 ns/iter (+/- 1)
// test string_search_1kb_safe_regex     ... bench:       4,416 ns/iter (+/- 170)
// test threaded_multi_regex             ... bench:  50,275,027 ns/iter (+/- 7,176,679)
// test threaded_multi_safe_regex        ... bench:     661,623 ns/iter (+/- 95,280)
// test threaded_single_regex            ... bench:  27,296,740 ns/iter (+/- 498,833)
// test threaded_single_safe_regex       ... bench:   3,368,486 ns/iter (+/- 207,483)
//
// test result: ok. 0 passed; 0 failed; 0 ignored; 44 measured; 0 filtered out; finished in 138.37s
#![allow(soft_unstable)]
#![feature(test)]
#![forbid(unsafe_code)]
extern crate test;
use regex::bytes::Regex;
use safe_regex::{regex, Matcher0, Matcher1, Matcher10, Matcher3, Matcher5};
use test::Bencher;

fn rand_bytes_without_z(n: usize) -> Vec<u8> {
    core::iter::from_fn(|| Some(rand::random()))
        .filter(|b| *b != b'Z' && *b != b'z')
        .take(n)
        .collect()
}

#[bench]
fn string_search_100_regex(b: &mut Bencher) {
    let re = Regex::new(r"2G8H81RFNZ").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.find(&data));
}

#[bench]
fn string_search_100_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br".*2G8H81RFNZ.*");
    let data: Vec<u8> = rand_bytes_without_z(100);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn string_search_1kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"2G8H81RFNZ").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.find(&data));
}

#[bench]
fn string_search_1kb_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br".*2G8H81RFNZ.*");
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn string_search_10kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"2G8H81RFNZ").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.find(&data));
}

#[bench]
fn string_search_10kb_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br".*2G8H81RFNZ.*");
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn string_search_100kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"2G8H81RFNZ").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(100 * 1024);
    b.iter(|| re.find(&data));
}

#[bench]
fn string_search_100kb_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br".*2G8H81RFNZ.*");
    let data: Vec<u8> = rand_bytes_without_z(100 * 1024);
    b.iter(|| re.match_slices(&data));
}

//////////////

#[bench]
fn repeat10_regex(b: &mut Bencher) {
    let re = Regex::new(r"^a{10,20}$").unwrap();
    b.iter(|| re.is_match(b"aaaaaaaaaa"));
}

#[bench]
fn repeat10_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br"a{10,20}");
    b.iter(|| re.match_slices(b"aaaaaaaaaa"));
}

#[bench]
fn repeat20_regex(b: &mut Bencher) {
    let re = Regex::new(r"^a{20,40}$").unwrap();
    b.iter(|| re.captures(b"aaaaaaaaaaaaaaaaaaaa"));
}

// Very very slow.  May never complete.
#[bench]
fn repeat20_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br"a{20,40}");
    b.iter(|| re.match_slices(b"aaaaaaaaaaaaaaaaaaaa"));
}

#[bench]
fn repeat30_regex(b: &mut Bencher) {
    let re = Regex::new(r"^a{30,60}$").unwrap();
    b.iter(|| re.is_match(b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
}

#[bench]
fn repeat30_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br"a{30,60}");
    b.iter(|| re.match_slices(b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
}

//////////////

#[bench]
fn repeat_capture10_regex(b: &mut Bencher) {
    let re = Regex::new(r"^(a{10,20})$").unwrap();
    b.iter(|| re.captures(b"aaaaaaaaaa"));
}

#[bench]
fn repeat_capture10_safe_regex(b: &mut Bencher) {
    let re: Matcher1<_> = regex!(br"(a{10,20})");
    b.iter(|| re.match_slices(b"aaaaaaaaaa"));
}

#[bench]
fn repeat_capture20_regex(b: &mut Bencher) {
    let re = Regex::new(r"^(a{20,40})$").unwrap();
    b.iter(|| re.captures(b"aaaaaaaaaaaaaaaaaaaa"));
}

#[bench]
fn repeat_capture20_safe_regex(b: &mut Bencher) {
    let re: Matcher1<_> = regex!(br"(a{20,40})");
    b.iter(|| re.match_slices(b"aaaaaaaaaaaaaaaaaaaa"));
}

#[bench]
fn repeat_capture30_regex(b: &mut Bencher) {
    let re = Regex::new(r"^(a{30,60})$").unwrap();
    b.iter(|| re.captures(b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
}

#[bench]
fn repeat_capture30_safe_regex(b: &mut Bencher) {
    let re: Matcher1<_> = regex!(br"(a{30,60})");
    b.iter(|| re.match_slices(b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
}

//////////////

#[bench]
fn capture10_regex(b: &mut Bencher) {
    let re = Regex::new(r"^(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)aaaaaaaaaa$").unwrap();
    b.iter(|| re.captures(b"aaaaaaaaaa"));
}

#[bench]
fn capture10_safe_regex(b: &mut Bencher) {
    let re: Matcher10<_> = regex!(br"(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)aaaaaaaaaa");
    b.iter(|| re.match_slices(b"aaaaaaaaaa"));
}

// #[bench]
// fn capture20_regex(b: &mut Bencher) {
//     let re = Regex::new(r"^(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)aaaaaaaaaaaaaaaaaaaa$").unwrap();
//     b.iter(|| re.captures(b"aaaaaaaaaaaaaaaaaaaa"));
// }
//
// #[bench]
// fn capture20_safe_regex(b: &mut Bencher) {
//     let re: Matcher<_> = regex!(br"(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)(a?)aaaaaaaaaaaaaaaaaaaa");
//     b.iter(|| re.match_slices(b"aaaaaaaaaaaaaaaaaaaa"));
// }

// TODO(mleonhard) Add tests for star: (a*)(a*)(a*), (a*)(b*)(c*), etc.

//////////////

#[bench]
fn datetime_capture_100_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(100);
    b.iter(|| re.captures(&data));
}

#[bench]
fn datetime_capture_100_safe_regex(b: &mut Bencher) {
    let re: Matcher5<_> = regex!(br".*([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+).*");
    let data: Vec<u8> = rand_bytes_without_z(100);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn datetime_capture_1kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn datetime_capture_1kb_safe_regex(b: &mut Bencher) {
    let re: Matcher5<_> = regex!(br".*([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+).*");
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn datetime_capture_10kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn datetime_capture_10kb_safe_regex(b: &mut Bencher) {
    let re: Matcher5<_> = regex!(br".*([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+).*");
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.match_slices(&data));
}

//////////////

const DATE_TIME: &[u8] = b"1999-12-32 23:59";
#[bench]
fn datetime_parse_regex(b: &mut Bencher) {
    let re = Regex::new(r"^([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)$").unwrap();
    b.iter(|| re.captures(DATE_TIME));
}

#[bench]
fn datetime_parse_safe_regex(b: &mut Bencher) {
    let re: Matcher5<_> = regex!(br"([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)");
    b.iter(|| re.match_slices(DATE_TIME));
}

//////////////

const PEM_BASE64_LINE: &[u8] = b"psGUNwWXrARgiInCeQkvN3toQrXOyQ5Df3MwrTAUIy0Nec7MrUEcdjrE0Mks3HhH";

#[bench]
fn pem_base64_regex(b: &mut Bencher) {
    let re = Regex::new(r"^[a-zA-Z0-9+/]{0,64}=*$").unwrap();
    b.iter(|| re.is_match(PEM_BASE64_LINE));
}

#[bench]
fn pem_base64_safe_regex(b: &mut Bencher) {
    let re: Matcher0<_> = regex!(br"[a-zA-Z0-9+/]{0,64}=*");
    b.iter(|| re.match_slices(PEM_BASE64_LINE));
}

//////////////

#[bench]
fn phone_capture_1kb_safe_regex(b: &mut Bencher) {
    let re: Matcher3<_> = regex!(br".*([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4}).*");
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn phone_capture_1kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn phone_capture_10kb_safe_regex(b: &mut Bencher) {
    let re: Matcher3<_> = regex!(br".*([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4}).*");
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn phone_capture_10kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(10 * 1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn phone_capture_100kb_safe_regex(b: &mut Bencher) {
    let re: Matcher3<_> = regex!(br".*([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4}).*");
    let data: Vec<u8> = rand_bytes_without_z(100 * 1024);
    b.iter(|| re.match_slices(&data));
}

#[bench]
fn phone_capture_100kb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(100 * 1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn phone_capture_1mb_regex(b: &mut Bencher) {
    let re = Regex::new(r"([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})").unwrap();
    let data: Vec<u8> = rand_bytes_without_z(1024 * 1024);
    b.iter(|| re.captures(&data));
}

#[bench]
fn phone_capture_1mb_safe_regex(b: &mut Bencher) {
    let re: Matcher3<_> = regex!(br".*([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4}).*");
    let data: Vec<u8> = rand_bytes_without_z(1024 * 1024);
    b.iter(|| re.match_slices(&data));
}

// "Contention on multi-threaded regex matching"
// https://morestina.net/blog/1827/multi-threaded-regex
fn threaded_multi_strings() -> Vec<&'static str> {
    ["foo", "bar", "1234", "1234foo", ""]
        .iter()
        .copied()
        .cycle()
        .take(1_000_000)
        .collect::<Vec<&'static str>>()
}
#[bench]
fn threaded_single_regex(b: &mut Bencher) {
    let re = Regex::new("^[0-9]+$").unwrap();
    let strings: Vec<&str> = threaded_multi_strings();
    b.iter(|| strings.iter().filter(|s| re.is_match(s.as_bytes())).count());
}

#[bench]
fn threaded_multi_regex(b: &mut Bencher) {
    let re = Regex::new("^[0-9]+$").unwrap();
    let strings: Vec<&str> = threaded_multi_strings();
    use rayon::prelude::*;
    b.iter(|| {
        strings
            .par_iter()
            .filter(|s| re.is_match(s.as_bytes()))
            .count()
    });
}

#[bench]
fn threaded_single_safe_regex(b: &mut Bencher) {
    let strings: Vec<&str> = threaded_multi_strings();
    let re: Matcher0<_> = regex!(br"^[0-9]+$");
    b.iter(|| strings.iter().filter(|s| re.is_match(s.as_bytes())).count());
}

#[bench]
fn threaded_multi_safe_regex(b: &mut Bencher) {
    let strings: Vec<&str> = threaded_multi_strings();
    let re: Matcher0<_> = regex!(br"^[0-9]+$");
    use rayon::prelude::*;
    b.iter(|| {
        strings
            .par_iter()
            .filter(|s| re.is_match(s.as_bytes()))
            .count()
    });
}
